package com.mashibing.debug.idea.example;

public class Printer {

    private boolean ready;

    Printer() {
        System.out.println();
    }

    public static void main(String[] args) {
        System.out.println("123");
    }

    void print1() {
        print2();
        System.out.println("print1 Printing to console");
    }

    void print2() {
        System.out.println("print2 Printing to console");
    }
}
