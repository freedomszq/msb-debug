package com.mashibing.debug.snowflake.model;

import lombok.Data;
import lombok.ToString;

/**
 * 学生
 *
 * @author sunzhiqiang
 * @date 2021/12/19 21:17:47
 */
@Data
@ToString
public class Student {
    private String name;

//    private ByteBuffer byteBuffer;
}
