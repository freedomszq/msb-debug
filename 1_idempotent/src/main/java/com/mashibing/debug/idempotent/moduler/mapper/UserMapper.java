package com.mashibing.debug.idempotent.moduler.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mashibing.debug.idempotent.moduler.entity.User;

public interface UserMapper extends BaseMapper<User> {

}
